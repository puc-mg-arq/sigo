package com.sigo.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Usuario implements Serializable {

    public static String USUARIO_VIEW = "usuario";

    @SerializedName("id")
    @JsonProperty(value = "id")
    private String id;

    @SerializedName("name")
    @JsonProperty(value = "name")
    private String nome;

    @SerializedName("user")
    @JsonProperty(value = "user")
    private String usuario;

    @SerializedName("key")
    @JsonProperty(value = "key")
    private String senha;

    private String token;

}
