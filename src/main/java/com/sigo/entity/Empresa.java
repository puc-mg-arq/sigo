package com.sigo.entity;

import lombok.Data;

@Data
public class Empresa {

    private Long id;

    private String cnpj;

    private String nome;

    private String telefone;

    public Empresa() {
    }

    public Empresa(String cnpj, String nome, String telefone) {
        this.cnpj = cnpj;
        this.nome = nome;
        this.telefone = telefone;
    }
}
