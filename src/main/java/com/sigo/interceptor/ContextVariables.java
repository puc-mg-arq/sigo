package com.sigo.interceptor;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
@Data
public class ContextVariables {

    @Value("${server.servlet.context-path:}")
    private String contextPath;

    @Value("${sigo.appName}")
    private String appName;

    @Override
    public String toString() {
        return "ContextVariables{" +
                "contextPath='" + contextPath + '\'' +
                ", appName='" + appName + '\'' +
                '}';
    }
}