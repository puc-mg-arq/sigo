package com.sigo.interceptor;

import com.google.gson.Gson;
import com.sigo.dataoobject.Login;
import com.sigo.dataoobject.UserCookie;
import com.sigo.entity.Usuario;
import com.sigo.support.EndpointSigoAuth;
import com.sigo.utils.CookieUtils;
import com.sigo.utils.OkHttp3Utils;
import com.sigo.utils.UserUtils;
import okhttp3.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Configuration
public class InterceptorUserSession extends HandlerInterceptorAdapter {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name = "gson")
    private Gson gson;

    @Resource(name = "okHttp3Utils")
    private OkHttp3Utils okHttp3Utils;

    @Resource(name = "endpointSigoAuth")
    private EndpointSigoAuth endpointSigoAuth;

    private final String LOGIN_REDIRECT = "/sigo/login/";

    private final String MESSAGE_ERROR_FIELD = "errors";

    private boolean urlPublic (HttpServletRequest request) {

        List<String> urls = new ArrayList<>();
        urls.add("/v2/api-docs");
        urls.add("/error");
        urls.add(LOGIN_REDIRECT);
//        urls.add("/user/v1/register");
        urls.add(".css");
        urls.add(".js");

        String requestURI = request.getRequestURI();
//        logger.log(String.format("request URI - %s", requestURI));
        for (String url: urls)
            if (requestURI.contains(url)) {
                logger.info(String.format("request URI: %s / contains: %s", requestURI, url));
                return true;
            }
//            else
//                logger.log(String.format("URL blocked - %s", url));

        return false;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        if (urlPublic(request)) {
            return true;
        }

        logger.info(String.format("Getting user into UserInterceptor"));
        String cookieNameUserSession = UserUtils.getCookieNameUserSession(request);
        String cookieJSONValue = CookieUtils.getCookieValue(request, cookieNameUserSession);

        if (StringUtils.isEmpty(cookieJSONValue)) {

            UserUtils.invalidateUserInCookie(request, response);
            logger.info("User not found in UserInterceptor.");
            response.sendRedirect(LOGIN_REDIRECT);
            return false;
        }

        UserCookie user = gson.fromJson(cookieJSONValue, UserCookie.class); //objectMapper.readValue(cookieJSONValue, User.class);

        Response resp = okHttp3Utils.doGet(endpointSigoAuth.getTokenValid(), Map.of("x-sigo-token", user.getToken(),
                "x-sigo-user", user.getUsuario()));
        if (resp.code() < 200 || resp.code() >= 300) {
            response.sendRedirect(LOGIN_REDIRECT);
            return false;
        }

        Login l = gson.fromJson(resp.body().string(), Login.class);
        if (!l.getResult().equals(user.getToken())) {
            response.sendRedirect(LOGIN_REDIRECT);
            return false;
        }

        logger.info(String.format("User found in UserInterceptor - %s", user.getNome()));
        UserUtils.putUserInCookie(request, response, user.userFromUserCookie(), user.getToken());

        logger.info("Cookie renew.");

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView view) throws Exception {

        Usuario userInCookie = UserUtils.getUserInCookie(request);
        if (!Objects.isNull(userInCookie) && !Objects.isNull(view)) {
            logger.info(String.format("User found in UserInterceptor (postHandle) - %s", userInCookie.getNome()));
            view.addObject(Usuario.USUARIO_VIEW, userInCookie);
        } else {
            if (!Objects.isNull(view))
                view.addObject(MESSAGE_ERROR_FIELD, "Por favor, efetue login.");
        }

        super.postHandle(request, response, handler, view);
    }
}
