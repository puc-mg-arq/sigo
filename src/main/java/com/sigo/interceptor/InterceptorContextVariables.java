package com.sigo.interceptor;

import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
public class InterceptorContextVariables extends HandlerInterceptorAdapter {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    final @NonNull ContextVariables contextVariables;

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {

        logger.info("contextPath: {}", contextVariables.getContextPath());
        if (modelAndView != null) {
            modelAndView.addObject("contextPath", contextVariables.getContextPath());
            modelAndView.addObject("appName", contextVariables.getAppName());
        }

        super.postHandle(request, response, handler, modelAndView);
    }

}
