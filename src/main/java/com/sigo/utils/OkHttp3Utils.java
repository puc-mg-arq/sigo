package com.sigo.utils;

import okhttp3.*;

import java.io.IOException;
import java.util.Map;

public class OkHttp3Utils {

    private OkHttpClient client;

    public OkHttp3Utils(OkHttpClient client) {
        this.client = client;
    }

    public Response doGet(String url, Map<String, String> headers) throws IOException {

        Request req = new Request.Builder()
                .url(url)
                .get()
                .headers(Headers.of(headers))
                .build();

        Response response = client.newCall(req).execute();

        return response;
    }

    public Response doPost(String url, Map<String, String> headers, Map<String, String> formParams) throws IOException {

        RequestBody body = this.getForm(formParams).build();
        Request req = new Request.Builder()
                .url(url)
                .post(body)
                .headers(Headers.of(headers))
                .build();

        Response response = client.newCall(req).execute();

        return response;
    }

    public Response doPost(String url, Map<String, String> headers, String json) throws IOException {

        RequestBody body = RequestBody.create(json, MediaType.get(org.springframework.http.MediaType.APPLICATION_JSON_VALUE));
        Request req = new Request.Builder()
                .url(url)
                .post(body)
                .headers(Headers.of(headers))
                .build();

        Response response = client.newCall(req).execute();

        return response;
    }

    public Response doDelete(String url, Map<String, String> headers, Map<String, String> formParams) throws IOException {

        RequestBody body = this.getForm(formParams).build();
        Request req = new Request.Builder()
                .url(url)
                .delete(body)
                .headers(Headers.of(headers))
                .build();

        Response response = client.newCall(req).execute();

        return response;
    }

    private FormBody.Builder getForm(Map<String, String> formParams) {

        FormBody.Builder form = new FormBody.Builder();

        if (!ValidacoesUtils.isEmpty(formParams))
            for (Map.Entry<String, String> param : formParams.entrySet())
                if (!ValidacoesUtils.isEmpty(param.getKey()))
                    form.add(param.getKey(), param.getValue());

        return form;
    }

}
