package com.sigo.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Calendar;

public class UtilsDate {

  public static Calendar stringToCalendar(String data) {
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    
    Calendar d = Calendar.getInstance();
    try {
    	
      d.setTime(sdf.parse(data));
    
    } catch (ParseException e) {
      e.printStackTrace();
    }
    
    return d;
  }

  public static String calendarToString (Calendar data) {
	  
	  if (data == null) return null;
	  
	  SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	  String d = sdf.format(data.getTime());
	  
	  return d;
  }

  public static String getTimestampFrom (Calendar date) {

    if (ValidacoesUtils.isEmpty(date))
      return "";

    String timeStamp =  new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date.getTime());

    return timeStamp;
  }

  public static String getTimestampFrom (LocalDateTime localDateTime) {

    if (localDateTime == null)
      return "";

    return Timestamp.valueOf(localDateTime).toString();
  }

}
