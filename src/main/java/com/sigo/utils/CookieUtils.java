package com.sigo.utils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Objects;

public class CookieUtils {

    public static Cookie getCookie(HttpServletRequest request, String cookieName) {

        if (!Objects.isNull(request.getCookies()))
            for (Cookie cookie : request.getCookies())
                if (cookie.getName().equals(cookieName))
                    return cookie;

        return new Cookie(cookieName, "");
    }

    public static String getCookieValue (HttpServletRequest request, String cookieName) {

        return getCookie(request, cookieName).getValue();
    }

    public static void invalidateCookie(Cookie cookie, HttpServletResponse response) {

        cookie.setMaxAge(0);
        cookie.setPath("/");
        cookie.setVersion(1);
        response.addCookie(cookie);

    }

}
