package com.sigo.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.Calendar;

public final class LoggerUtils {

    private HttpServletRequest request;

    private Logger logger;

    private Class<?> clazz;

    private String date;

    private String serverName;

    private String uri;

    private static String MESSAGE_LOG_DEFAULT = "{ class: {}, ServerName: {}, URI: {}, date: {}, msg: {} }";
//    private static String MESSAGE_LOG_DEFAULT_SERVICE = "{ class: {}, URI: {}, date: {}, user: {}, msg: {}, idService: {} }";

    public LoggerUtils (Class<?> clazz) {

        this.clazz = clazz;
        this.logger = LoggerFactory.getLogger(this.clazz);
        this.request = request;

    }

    public LoggerUtils addClass (Class<?> clazz) {

        this.clazz = clazz;
        this.logger = LoggerFactory.getLogger(this.clazz);

        return this;
    }

    public LoggerUtils addRequest (HttpServletRequest request) {

        this.request = request;

        return this;
    }

    public void log (String msg) {

        this.date = UtilsDate.getTimestampFrom(LocalDateTime.now());
        this.serverName = request.getServerName();
        this.uri = request.getRequestURI();
        logger.info(MESSAGE_LOG_DEFAULT, this.clazz, this.serverName, this.uri, this.date, msg);

    }

}
