package com.sigo.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sigo.dataoobject.UserCookie;
import com.sigo.entity.Usuario;
import org.springframework.util.StringUtils;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class UserUtils {

    public static String getCookieNameUserSession(HttpServletRequest request) {
        StringBuilder cookieName = new StringBuilder(request.getServerName());
        cookieName.append("-").append("user");

        return cookieName.toString();
    }

    public static void putUserInCookie(HttpServletRequest request, HttpServletResponse response, Usuario user, String token) throws JsonProcessingException {

        ObjectMapper objectMapper = new ObjectMapper();
        UserCookie userCookie = new UserCookie();
        userCookie.userToUserCookie(user, token);
        Cookie cookie = new Cookie(UserUtils.getCookieNameUserSession(request), objectMapper.writeValueAsString(userCookie));
        cookie.setMaxAge((int) TimeUnit.MINUTES.toSeconds(60));
        cookie.setPath("/");
        cookie.setVersion(1);
        response.addCookie(cookie);

    }

    public static Usuario getUserInCookie(HttpServletRequest request) throws IOException {

        ObjectMapper objectMapper = new ObjectMapper();
        String cookieJSONValue = CookieUtils.getCookieValue(request, UserUtils.getCookieNameUserSession(request));

        if (StringUtils.isEmpty(cookieJSONValue))
            return null;

        UserCookie userCookie = objectMapper.readValue(cookieJSONValue, UserCookie.class);
        Usuario user = userCookie.userFromUserCookie();

        return user;
    }

    public static void invalidateUserInCookie(HttpServletRequest request, HttpServletResponse response) throws IOException {

        Cookie cookie = new Cookie(UserUtils.getCookieNameUserSession(request), "");
        cookie.setMaxAge(0);
        cookie.setPath("/");
        cookie.setVersion(1);
        response.addCookie(cookie);
    }

}
