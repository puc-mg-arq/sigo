package com.sigo.utils;

import java.util.Locale;
import java.util.TimeZone;

public interface ILocalidade {
	
	TimeZone TIMEZONE = TimeZone.getTimeZone("America/Sao_Paulo");
	Locale LocaleBR = new Locale("pt", "BR");
	
	Locale DEFAULT_LOCALE = LocaleBR;
	
}
