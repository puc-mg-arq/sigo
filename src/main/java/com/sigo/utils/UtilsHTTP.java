package com.sigo.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import okhttp3.Response;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

public class UtilsHTTP {

    @Resource(name = "objectMapper")
    private ObjectMapper objectMapper;

    public static String getHeader (String key, HttpServletRequest request) {

        if (ValidacoesUtils.isEmpty(key))
            return "";

        return request.getHeader(key);
    }

    public static String getJSONFromResponse (Response response) throws IOException {

        int code = response.code();
        String json = response.body().string();

        if (code >= 300) {

//            ValidationError validationError = objectMapper.readValue(json, ValidationError.class);
//            throw new ValidationException(validationError.getErros(), StatusMessage.ACESSO_DENIED);
            return "";
        }

        return json;
    }

}
