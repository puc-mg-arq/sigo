package com.sigo.utils;

import java.util.Random;

public class Utils {


	public static String replaceCaracteresEspeciais (String s) {
		
		if (ValidacoesUtils.isEmpty(s)) return "";
		
		s = s.trim().replace(".", "").replace("/", "").replace("-", "").replace("[", "").replace("]", "");
		
		return s;
		
	}
	
	public static String formatarCpfCnpj (String cpfCnpj) {

		if (ValidacoesUtils.isEmpty(cpfCnpj)) return "";		
		
		StringBuilder apresentacao = new StringBuilder(Utils.replaceCaracteresEspeciais(cpfCnpj));
		
		if (apresentacao.length() == 11) {

			apresentacao.insert(apresentacao.length() - 2, "-")
			            .insert(apresentacao.length() - 6, ".")
			            .insert(apresentacao.length() - 10, ".");
			
		} else if (apresentacao.length() == 14) {

			apresentacao.insert(apresentacao.length() - 2, "-")
			            .insert(apresentacao.length() - 7, "/")
			            .insert(apresentacao.length() - 11, ".")
			            .insert(apresentacao.length() - 15, ".");
			
		}
		
		return apresentacao.toString();
	}
	
	public static String formatarTelefone (String telefone) {
		
		if (ValidacoesUtils.isEmpty(telefone)) return "";		
		
		StringBuilder apresentacao = new StringBuilder(Utils.replaceCaracteresEspeciais(telefone));	

		apresentacao.insert(apresentacao.length() - 4, "-")
                    .insert(2, ")")
                    .insert(0, "(");
		
		return apresentacao.toString();
	}
	
	public static String formatarCEP (String cep) {
		
		if (ValidacoesUtils.isEmpty(cep)) return "";		
		
		StringBuilder apresentacao = new StringBuilder(Utils.replaceCaracteresEspeciais(cep));	

		apresentacao.insert(apresentacao.length() - 3, "-")
                    .insert(apresentacao.length() - 7, ".");
		
		return apresentacao.toString();
	}
	
	public static String gerarCaracteresRandomicos(int tamanhoCaracteres) {

		char[] letras = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_-".toCharArray();
		String result = "";
		
		Random random = new Random();
		
		do {
			
			int nextLong = random.nextInt(letras.length);
			
			String letra = String.valueOf(letras[nextLong]);
//			if (result.contains(letra)) {
//				continue;
//			}
			
			result += letra;			
			
		} while (result.length() < tamanhoCaracteres);
		
		return result;
	}
	
}
