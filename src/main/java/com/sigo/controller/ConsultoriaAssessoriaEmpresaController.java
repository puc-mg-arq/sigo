package com.sigo.controller;


import com.google.gson.Gson;
import com.sigo.dataoobject.UserCookie;
import com.sigo.entity.Empresa;
import com.sigo.entity.Usuario;
import com.sigo.service.EmpresaService;
import com.sigo.support.EndpointSigoAuth;
import com.sigo.utils.CookieUtils;
import com.sigo.utils.OkHttp3Utils;
import com.sigo.utils.UserUtils;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@Controller
@RequiredArgsConstructor
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@RequestMapping(path = {"consultoria/empresa"})
public class ConsultoriaAssessoriaEmpresaController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    private final String HOME_VIEW = "consultoriaAssessoria/empresa/home";
    private final String CADASTRO_VIEW = "consultoriaAssessoria/empresa/cadastro";
    private final String SUCESSO_ACAO = "Sucesso!!";

    ModelAndView view = new ModelAndView();

    @Resource(name = "gson")
    private Gson gson;

    @Resource(name = "okHttp3Utils")
    private OkHttp3Utils okHttp3Utils;

    @Resource(name = "endpointSigoAuth")
    private EndpointSigoAuth endpointSigoAuth;

    private final @NonNull HttpServletRequest request;

    private final @NonNull HttpServletResponse response;

    private final @NonNull EmpresaService empresaService;

    @RequestMapping(path = {"/home"}, method = {RequestMethod.GET})
    public ModelAndView home () throws IOException {

        Usuario userInCookie = UserUtils.getUserInCookie(request);
        Collection<Empresa> empresas = empresaService.getAll("", "", userInCookie);
//        List emps = new ArrayList();
//        emps.addAll(empresas);
//        Collections.reverse(emps);
        view.addObject("empresas", empresas).setViewName(HOME_VIEW);

        return view;
    }

    @RequestMapping(path = {"/edit/{idEmpresa}"}, method = {RequestMethod.GET})
    public ModelAndView editView (@PathVariable(name = "idEmpresa") String idEmpresa) throws IOException {

        Empresa empresa = empresaService.getByID(idEmpresa, UserUtils.getUserInCookie(request));
        view.addObject("empresa", empresa).setViewName(CADASTRO_VIEW);

        return view;
    }

    @RequestMapping(path = {"/consulta"}, method = {RequestMethod.GET})
    public ModelAndView consulta (@RequestParam(name = "cnpj") String cnpj, @RequestParam(name = "nome") String nome) throws IOException {

        Usuario userInCookie = UserUtils.getUserInCookie(request);
        Collection<Empresa> empresas = empresaService.getAll(nome, cnpj, userInCookie);
        view.addObject("empresas", empresas).setViewName(HOME_VIEW);

        return view;
    }

    @RequestMapping(path = {"/cadastrar"}, method = {RequestMethod.GET})
    public ModelAndView cadastrarView (){

        view.setViewName(CADASTRO_VIEW);

        return view;
    }

    @RequestMapping(path = {"/cadastrar"}, method = {RequestMethod.POST})
    public ModelAndView cadastrar (Empresa empresa) throws IOException {

        Usuario userInCookie = UserUtils.getUserInCookie(request);
        empresaService.cadastrar(empresa, userInCookie);

//        logger.info("Text: {}", text);
        view.addObject("success", SUCESSO_ACAO);

        return this.home();
    }

    @RequestMapping(path = {"/remover/{idEmpresa}"}, method = {RequestMethod.GET})
    public ModelAndView remover (@PathVariable(name = "idEmpresa") String idEmpresa) throws IOException {

        Usuario userInCookie = UserUtils.getUserInCookie(request);
        Collection<Empresa> empresas = empresaService.remover(idEmpresa, userInCookie);

//        logger.info("Text: {}", text);
        view.addObject("empresas", empresas)
                .addObject("success", SUCESSO_ACAO).
                setViewName(HOME_VIEW);

        return view;
    }

}
