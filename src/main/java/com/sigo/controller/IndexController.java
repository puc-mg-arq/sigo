package com.sigo.controller;


import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RequiredArgsConstructor
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@Controller
public class IndexController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    ModelAndView view = new ModelAndView();

    private final @NonNull HttpServletRequest request;

    private final @NonNull HttpServletResponse response;

    @RequestMapping(path = {"", "/"})
    public ModelAndView index () {

        view.setViewName("redirect:/login/");
        return view;
    }

    @RequestMapping(path = {"hello"})
    public ModelAndView hello (@RequestParam(name = "text",
            required = false,
            defaultValue = "Hello World!") String text) {

        logger.info(text);
        view.addObject("text", text).
                setViewName("/hello/hello");

        return view;
    }

    @RequestMapping(path = {"/clear-cookies"})
    public ModelAndView clearCookies () {

//        Cookie cookie = CookieUtils.getCookie(request, UserUtils.getCookieNameUserSession(request));
//        CookieUtils.invalidateCookie(cookie, response);
        view.setViewName("redirect:/user/v1");
        return view;
    }

}
