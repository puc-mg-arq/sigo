package com.sigo.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.sigo.dataoobject.Login;
import com.sigo.dataoobject.UserCookie;
import com.sigo.entity.Usuario;
import com.sigo.support.EndpointSigoAuth;
import com.sigo.utils.CookieUtils;
import com.sigo.utils.OkHttp3Utils;
import com.sigo.utils.UserUtils;
import com.sigo.utils.UtilsHTTP;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequiredArgsConstructor
@Scope(value = WebApplicationContext.SCOPE_REQUEST)
@RequestMapping(path = {"login/"})
public class LoginController {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    ModelAndView view = new ModelAndView();

    @Resource(name = "gson")
    private Gson gson;

    @Resource(name = "okHttp3Utils")
    private OkHttp3Utils okHttp3Utils;

    @Resource(name = "endpointSigoAuth")
    private EndpointSigoAuth endpointSigoAuth;

    private final @NonNull HttpServletRequest request;

    private final @NonNull HttpServletResponse response;

    @RequestMapping(path = {"", "/"}, method = {RequestMethod.GET})
    public ModelAndView loginView (@RequestParam(name = "text",
            required = false,
            defaultValue = "Hello World!") String text) {

        logger.info("Text: {}", text);
        view.addObject("text", text).
                setViewName("login/login");

        return view;
    }

    @RequestMapping(path = {"", "/"}, method = {RequestMethod.POST})
    public ModelAndView login (@RequestParam(name = "usuario",
            required = false) String usuario, @RequestParam(name = "senha",
            required = false) String senha) throws IOException {

        logger.info("usuario: {} - senha: {}", usuario, senha);

        Map<String, String> headers = Map.of("x-sigo-user", usuario, "x-sigo-key", senha);

        Response resp = okHttp3Utils.doGet(endpointSigoAuth.getLogin(), headers);
        String jsonFromResponse = UtilsHTTP.getJSONFromResponse(resp);
        if (StringUtils.isBlank(jsonFromResponse)) {

            view.setViewName("login/login");

            return view;
        }

        Login l = gson.fromJson(jsonFromResponse, Login.class);
        UserUtils.putUserInCookie(request, response, l.getUsuario(), l.getResult());

        view.addObject(Usuario.USUARIO_VIEW, l.getUsuario()).setViewName("login/home");

        return this.home();
    }

    @RequestMapping(path = {"/home"})
    public ModelAndView home () throws IOException {

        view.setViewName("login/home");

        return view;
    }

    @RequestMapping(path = "/logout", method = {RequestMethod.GET})
    public ModelAndView logout () {

        Cookie cookie = CookieUtils.getCookie(request, UserUtils.getCookieNameUserSession(request));
        CookieUtils.invalidateCookie(cookie, response);

        return this.loginView("");
    }

}
