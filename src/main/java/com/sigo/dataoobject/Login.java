package com.sigo.dataoobject;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import com.sigo.entity.Usuario;
import lombok.Data;

@JsonIgnoreProperties(ignoreUnknown = true)
@Data
public class Login {

    private String result;

    @SerializedName("user")
    @JsonProperty(value = "user")
    private Usuario usuario;

}
