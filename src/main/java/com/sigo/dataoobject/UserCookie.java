package com.sigo.dataoobject;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.sigo.entity.Usuario;
import lombok.Data;

@Data
public class UserCookie {

    private String id;

    private String token;

    private String nome;

    private String usuario;

    @JsonIgnore
    public Usuario userFromUserCookie () {

        Usuario user = new Usuario();
        user.setId(this.getId());
        user.setNome(this.getNome());
        user.setToken(this.getToken());
        user.setUsuario(this.getUsuario());

        return user;
    }

    @JsonIgnore
    public void userToUserCookie (Usuario user, String token) {

        this.setId(user.getId());
        this.setNome(user.getNome());
        this.setToken(token);
        this.setUsuario(user.getUsuario());

    }

}
