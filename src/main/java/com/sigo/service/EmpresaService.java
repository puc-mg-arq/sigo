package com.sigo.service;

import com.google.gson.Gson;
import com.sigo.dataoobject.UserCookie;
import com.sigo.entity.Empresa;
import com.sigo.entity.Usuario;
import com.sigo.support.EndpointSigoConsultoriaAssessoria;
import com.sigo.utils.CookieUtils;
import com.sigo.utils.OkHttp3Utils;
import com.sigo.utils.UserUtils;
import okhttp3.Response;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class EmpresaService {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Resource(name = "gson")
    private Gson gson;

    @Resource(name = "DBEmpresa")
    private Map<String, Empresa> DBEmpresa;

    @Resource(name = "okHttp3Utils")
    private OkHttp3Utils okHttp3Utils;

    @Resource(name = "endpointSigoConsultoriaAssessoria")
    private EndpointSigoConsultoriaAssessoria endpointSigoConsultoriaAssessoria;

    public Empresa cadastrar(Empresa empresa, Usuario usuario) {

//        if (StringUtils.isBlank(empresa.getId())) {
//            empresa.setId(UUID.randomUUID().toString());
//        }
//
//        DBEmpresa.put(empresa.getId(), empresa);
//
//        DBEmpresa.forEach((k, v) -> logger.info("ID: {} -> Empresa: {}", k, v.toString()));

        try {

            Map<String, String> headers = Map.of("x-sigo-token", usuario.getToken(),
                    "x-sigo-user", usuario.getUsuario());

            String empJson = gson.toJson(empresa);
            Response response = okHttp3Utils.doPost(endpointSigoConsultoriaAssessoria.getRegistrar(), headers, empJson);
            String body = response.body().string();

            empresa = gson.fromJson(body, Empresa.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return empresa;
    }

    public Collection<Empresa> remover(String ID, Usuario usuario) {

//        DBEmpresa.remove(ID);
//        Collection<Empresa> values = this.DBEmpresa.values();

        Collection<Empresa> values = null;
        try {

            Map<String, String> headers = Map.of("x-sigo-token", usuario.getToken(),
                    "x-sigo-user", usuario.getUsuario());

            String url = endpointSigoConsultoriaAssessoria.getRemover().replace("%ID%", ID);
            Response response = okHttp3Utils.doDelete(url, headers, Map.of());

            values = this.getAll("", "", usuario);

        } catch (IOException e) {
            e.printStackTrace();
        }


        return values;
    }

    public Collection<Empresa> getAll(String nome, String cnpj, Usuario usuario) {

//        Collection<Empresa> values = this.DBEmpresa.values();

        Collection<Empresa> values = null;

        try {

            Map<String, String> headers = Map.of("x-sigo-token", usuario.getToken(),
                    "x-sigo-user", usuario.getUsuario());

            String url = endpointSigoConsultoriaAssessoria.getGetBy() + "?";
            url += StringUtils.defaultIfBlank("&nome=" + nome, "");
            url += StringUtils.defaultIfBlank("&cnpj=" + cnpj, "");
            Response response = okHttp3Utils.doGet(url, headers);
            String body = response.body().string();

            values = Arrays.asList(gson.fromJson(body, Empresa[].class));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return values;
    }

    public Empresa getByID(String ID, Usuario usuario) {

//        Empresa empresa = this.DBEmpresa.get(ID);
        Empresa empresa = null;
        try {

            Map<String, String> headers = Map.of("x-sigo-token", usuario.getToken(),
                    "x-sigo-user", usuario.getUsuario());

            String url = endpointSigoConsultoriaAssessoria.getGetByID().replace("%ID%", ID);
            Response response = okHttp3Utils.doGet(url, headers);
            String body = response.body().string();

            empresa = gson.fromJson(body, Empresa.class);

        } catch (IOException e) {
            e.printStackTrace();
        }

        return empresa;
    }

    public Collection<Empresa> get(String nome, String cnpj) {

        List<Empresa> empresas = new ArrayList<>();

        if (!StringUtils.isBlank(cnpj) && !StringUtils.isBlank(nome)) {

            empresas.addAll(this.DBEmpresa.values()
                    .stream()
                    .filter(empresa -> empresa.getCnpj().equals(cnpj) && empresa.getNome().contains(nome))
                    .collect(Collectors.toList()));

            return empresas;
        }

        if (!StringUtils.isBlank(cnpj)) {

            empresas.addAll(this.DBEmpresa.values()
                    .stream()
                    .filter(empresa -> empresa.getCnpj().equals(cnpj))
                    .collect(Collectors.toList()));

            return empresas;
        }

        if (!StringUtils.isBlank(nome)) {

            empresas.addAll(this.DBEmpresa.values()
                    .stream()
                    .filter(empresa -> empresa.getNome().contains(nome))
                    .collect(Collectors.toList()));

            return empresas;
        }

        return empresas;
    }
}
