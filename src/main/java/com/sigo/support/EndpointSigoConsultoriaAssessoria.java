package com.sigo.support;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@Configuration
@ConfigurationProperties(prefix = "sigo-api-gateway.consultoria-assessoria.endpoint")
public class EndpointSigoConsultoriaAssessoria {

    private String registrar;

    private String getBy;

    private String getByID;

    private String remover;

}
