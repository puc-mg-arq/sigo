package com.sigo.support;

import lombok.Data;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "sigo-api-gateway.auth.endpoint")
public class EndpointSigoAuth {

    @Value("${sigo-api-gateway.login:}")
    private String login;

    private String tokenValid;

    private String tokenCreate;

    private String userCreate;

}
