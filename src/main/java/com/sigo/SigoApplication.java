package com.sigo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.samskivert.mustache.Mustache;
import com.sigo.entity.Empresa;
import com.sigo.interceptor.ContextVariables;
import com.sigo.interceptor.InterceptorUserSession;
import com.sigo.interceptor.InterceptorContextVariables;
import com.sigo.service.EmpresaService;
import com.sigo.support.EndpointSigoAuth;
import com.sigo.support.EndpointSigoConsultoriaAssessoria;
import com.sigo.utils.OkHttp3Utils;
import okhttp3.ConnectionPool;
import okhttp3.OkHttpClient;
import org.apache.tomcat.util.http.LegacyCookieProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.mustache.MustacheEnvironmentCollector;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.WebServerFactoryCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.concurrent.TimeUnit;

@SpringBootApplication
public class SigoApplication implements CommandLineRunner {

    Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private ContextVariables contextVariablesLoader;

    @Autowired
    private InterceptorUserSession interceptorUserSession;

    @Autowired
    private EmpresaService empresaService;

    @Autowired
    private EndpointSigoAuth endpointSigoAuth;

    @Autowired
    private EndpointSigoConsultoriaAssessoria endpointSigoConsultoriaAssessoria;

    private static Map<String, Empresa> DBEmpresa;

    public static void main(String[] args) {

        SpringApplication app = new SpringApplication(SigoApplication.class);
        app.run();

    }

    private Collection<Empresa> getEmpresasParaDB () {

        List<Empresa> empresas = new ArrayList<>();
        empresas.add(new Empresa("22.364.923/0001-65", "Empresa 01", "21999999999"));
        empresas.add(new Empresa("42.126.034/0001-77", "Empresa 02", "21988888888"));
        empresas.add(new Empresa("18.506.663/0001-48", "Empresa 03", "21977777777"));

        return empresas;
    }

    @Bean(name = "DBEmpresa")
    public Map<String, Empresa> getDBEmpresa() {
        DBEmpresa = new HashMap<>();

        return DBEmpresa;
    }

    @Bean(name = "okHttp3Utils")
    public OkHttp3Utils okHttp3Utils() {

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.SECONDS)
                .writeTimeout(5, TimeUnit.SECONDS)
                .readTimeout(5, TimeUnit.SECONDS)
                .build();

        return new OkHttp3Utils(client);
    }

    @Bean(name = "objectMapper")
    public ObjectMapper getObjectMapper() {
        ObjectMapper obj = new ObjectMapper();
        return obj;
    }

    @Bean(name = "gson")
    public Gson getGson() {
        Gson obj = new Gson();
        return obj;
    }

    /**
     * Solve the problem:
     * There was an unexpected error (type=Internal Server Error, status=500).
     * An invalid domain [.xxx.com] was specified for this cookie
     *
     * @return
     */
    @Bean
    public WebServerFactoryCustomizer<TomcatServletWebServerFactory> cookieProcessorCustomizer() {
        return (factory) -> factory.addContextCustomizers(
                (context) -> context.setCookieProcessor(new LegacyCookieProcessor()));
    }

    @Bean
    public Mustache.Compiler mustacheCompiler(
            Mustache.TemplateLoader templateLoader,
            Environment environment) {

        MustacheEnvironmentCollector collector
                = new MustacheEnvironmentCollector();
        collector.setEnvironment(environment);

        return Mustache.compiler()
                .defaultValue("")
                .withLoader(templateLoader)
                .withCollector(collector);
    }

    @Bean(name = "contextVariablesLoader")
    public InterceptorContextVariables ContextVariablesLoader() {

        InterceptorContextVariables icv = new InterceptorContextVariables(contextVariablesLoader);

        return icv;
    }

    @Override
    public void run(String... args) {

        logger.info("Load Project SIGO");
        logger.info("contextVariablesLoader: {}", contextVariablesLoader);
        logger.info("endpointSigoAuth: {}", endpointSigoAuth);
        logger.info("endpointSigoConsultoriaAssessoria: {}", endpointSigoConsultoriaAssessoria);

//        this.getEmpresasParaDB().forEach(emp -> empresaService.cadastrar(emp));
    }
}