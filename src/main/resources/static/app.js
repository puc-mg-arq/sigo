// let contextPath = document.currentScript.getAttribute('data-context-path');
const serviceWorker = contextPath + "/serviceWorker.js";
console.log("serviceWorker path: " + serviceWorker);
if ("serviceWorker" in navigator) {
    window.addEventListener("load", function() {
        navigator.serviceWorker
            .register(serviceWorker)
            .then(res => console.log("service worker registered: " + serviceWorker))
            .catch(err => console.log("service worker not registered: " + serviceWorker, err))
    })
}