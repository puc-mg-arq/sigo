#makefile

#References:
#	https://stackoverflow.com/questions/20763629/test-whether-a-directory-exists-inside-a-makefile
#VERSION := $(shell mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
#VER := $(shell echo $(VERSION) | perl -pe 's/-SNAPSHOT//')

APPLICATION_NAME := puc_mg_arq/sigo

build:
	mvn clean
	mvn release:update-versions
	mvn package -Dmaven.test.skip=true
	docker build --build-arg SPRING_PROFILES_ACTIVE=dev -f Dockerfile -t $(APPLICATION_NAME):latest .
# 	sudo docker build -f Dockerfile -t authentication-api:latest .
	docker build --build-arg SPRING_PROFILES_ACTIVE=dev -f Dockerfile -t $(APPLICATION_NAME):$(shell echo $(shell mvn help:evaluate -Dexpression=project.version -q -DforceStdout) | perl -pe 's/-SNAPSHOT//') .

test:
#	echo $(VERSION)
#	echo $(VER)
#	VERSION=$(shell mvn help:evaluate -Dexpression=project.version -q -DforceStdout)
	$(VER)

run-build:	build
	# sudo docker run -p 8081:8081 -t authentication-api:$(version)
	docker run -e SPRING_PROFILE=heroku -p 8090:8090 -t $(APPLICATION_NAME):latest

run:
	# sudo docker run -p 8081:8081 -t authentication-api:$(version)
	docker run -e SPRING_PROFILE=dev -p 8090:8090 -t $(APPLICATION_NAME):latest

deploy-heroku:
# 	sudo docker rmi -f registry.heroku.com/msc-authentication-api/web:latest
	mvn clean && mvn package
	sudo heroku container:push web -a $(APPLICATION_NAME)
	sudo heroku container:release web -a $(APPLICATION_NAME)

docker-clean-all:
	#To clear containers:
	docker rm -f $(docker ps -a -q) \
	#To clear images:
	docker rmi -f $(docker images -a -q) \
	#To clear volumes:
	docker volume rm $(docker volume ls -q) \
	#To clear networks:
	docker network rm $(docker network ls | tail -n+2 | awk '{if($2 !~ /bridge|none|host/){ print $1 }}')